#include "ColorHistogramDetector.h"

#include <fstream>
#include <string>


void castImageToColorModel(cv::Mat &image, const std::string &colorModel) {
    auto code = conversionCode.find(colorModel);
    if (code != conversionCode.end()) {
        cv::cvtColor(image, image, code->second);
    }
}


ColorHistogramDetector::ColorHistogramDetector(int histResolution, const std::string &colorModel)
    : colorModel(colorModel),
      objectHist(histResolution),
      colorHist(histResolution),
      objectFrequency(0) {}


void ColorHistogramDetector::setObjectFrequency(double freq) {
    objectFrequency = freq;
}

void ColorHistogramDetector::segment(const cv::Mat &bgrImage, cv::Mat &output) {
    cv::Mat castedImage;
    bgrImage.copyTo(castedImage);
    castImageToColorModel(castedImage, colorModel);

    initOutput(output, bgrImage.size());

    for (int i = 0; i < bgrImage.rows; ++i) {
        for (int k = 0; k < bgrImage.cols; ++k) {
            auto color = castedImage.at<cv::Vec3b>(i, k);
            auto colorFrequency = colorHist.getByColor(color);
            double prob = objectFrequency * objectHist.getByColor(color) / colorFrequency;
            bool pixelBelongsToObject = prob > 0.5;
            if (pixelBelongsToObject) {
                output.at<uchar>(i, k) = kObjectMask;
            }
        }
    }
}


std::ostream &operator<<(std::ostream &outputStream, const ColorHistogramDetector &model) {
    outputStream << model.colorModel << "\n"
        << model.objectFrequency << "\n"
        << model.objectHist << "\n"
        << model.colorHist;
    return outputStream;
}


std::istream &operator>>(std::istream &inputStream, ColorHistogramDetector &model) {
    inputStream >> model.colorModel
        >> model.objectFrequency
        >> model.objectHist
        >> model.colorHist;
    return inputStream;
}


void train(ColorHistogramDetector &model, const HistogramBuilder &histBuilder) {
    cv::Mat image;
    cv::Mat mask;
    unsigned int objectPixels = 0;
    unsigned int totalPixels = 0;

    while (readSample(image, mask)) {
        castImageToColorModel(image, model.colorModel);
        for (int i = 0; i < mask.rows; ++i) {
            uchar *row = mask.ptr(i);
            for (int k = 0; k < mask.cols; ++k) {
                if (kObjectMask == row[k] || kNonObjectMask == row[k]) {
                    ++totalPixels;
                    auto color = image.at<cv::Vec3b>(i, k);
                    histBuilder.strengthenColor(color, model.colorHist);
                    if (kObjectMask == row[k]) {
                        histBuilder.strengthenColor(color, model.objectHist);
                        ++objectPixels;
                    }
                }
            }
        }
    }

    model.objectFrequency = (Histogram::valueT) objectPixels / totalPixels;

    model.objectHist.normalize();
    model.colorHist.normalize();
}


cv::Ptr<Detector> ColorHistogramDetector::load(const std::string &modelPath) {
    std::ifstream modelFile(modelPath);
    cv::Ptr<ColorHistogramDetector> detector = modelFile ? cv::makePtr<ColorHistogramDetector>(1, "BGR") : nullptr;
    if (nullptr != detector) {
        modelFile >> *detector;
    }
    return detector;
}