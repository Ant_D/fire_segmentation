#ifndef FIRE_SEGMENTATION_COLORHISTOGRAMDETECTOR_H
#define FIRE_SEGMENTATION_COLORHISTOGRAMDETECTOR_H

#include <iostream>
#include <map>

#include <opencv2/imgproc/imgproc.hpp>

#include "Detector.h"
#include "input.h"
#include "histogramBuilders.h"


static std::map<std::string, cv::ColorConversionCodes> conversionCode = {
    {"HSV", cv::ColorConversionCodes::COLOR_BGR2HSV},
    {"YCrCb", cv::ColorConversionCodes::COLOR_BGR2YCrCb},
    {"YUV", cv::ColorConversionCodes ::COLOR_BGR2YUV}
};


class ColorHistogramDetector : public Detector {
public:
    static cv::Ptr<Detector> load(const std::string &modelPath);

public:
    ColorHistogramDetector(int histResolution, const std::string &colorModel);
    void setObjectFrequency(double freq);
    void segment(const cv::Mat &bgrImage, cv::Mat &output) final;

    friend std::ostream &operator<<(std::ostream &outputStream, const ColorHistogramDetector &model);
    friend std::istream &operator>>(std::istream &inputStream, ColorHistogramDetector &model);
    friend void train(ColorHistogramDetector &model, const HistogramBuilder &histBuilder);

private:
    std::string colorModel;
    Histogram objectHist;
    Histogram colorHist;
    double objectFrequency;
};

void train(ColorHistogramDetector &model, const HistogramBuilder &histBuilder);

#endif