﻿#include <fstream>
#include <iostream>

#include <opencv2/core/utility.hpp>

#include "histogramBuilders.h"
#include "ColorHistogramDetector.h"

using namespace std;

const char *kOptions =
    "{help h usage      |       | print help info}"
    "{color_model m     | BGR   | color model used for training}"
    "{model_path p      | model | path for saving of model}"
    "{hist_resolution r | 8     | histogram resolution}"
    "{filter_size f     | 5     | size of gaussian filter used in updating histograms}";


int main(int argc, const char **argv) {
    ios_base::sync_with_stdio(false);

    cv::CommandLineParser parser(argc, argv, kOptions);
    parser.about("Train histogram-based model");

    if (parser.has("help")) {
        parser.printMessage();
        return 0;
    }

    int histResolution = parser.get<int>("hist_resolution");
    int filterSize = parser.get<int>("filter_size");
    string modelPath = parser.get<string>("model_path");
    string colorModel = parser.get<string>("color_model");

    if (!parser.check()) {
        parser.printErrors();
        return EXIT_FAILURE;
    }

    cout << "Used parameters:" << "\n";
    cout << "\thistResolution == " << histResolution << "\n";
    cout << "\tfilterSize == " << filterSize << "\n";
    cout << "\tmodelPath == " << modelPath << "\n";
    cout << "\tcolorModel == " << colorModel << endl;

    ColorHistogramDetector model(histResolution, colorModel);
    GaussianNaiveBayesBuilder histBuilder(filterSize);

	train(model, histBuilder);

	ofstream modelFile(modelPath);
	modelFile << model;

	return 0;
}