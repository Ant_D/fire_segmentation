#ifndef FIRE_SEGMENTATION_DETECTOR_H
#define FIRE_SEGMENTATION_DETECTOR_H

#include <opencv2/core/core.hpp>


const uchar kObjectMask = 255;
const uchar kNonObjectMask = 0;


class Detector {
public:
    virtual ~Detector() = default;
    virtual void segment(const cv::Mat &bgrImage, cv::Mat &output) = 0;

protected:
    static void initOutput(cv::Mat &output, const cv::Size &size);
};

#endif //FIRE_SEGMENTATION_DETECTOR_H
