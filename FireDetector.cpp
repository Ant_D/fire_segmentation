#include "FireDetector.h"

#include <opencv2/imgproc.hpp>


FireDetector::FireDetector(cv::Ptr<Detector> pColorDetector, cv::Ptr<Detector> pMotionDetector)
    : pColorDetector(pColorDetector), pMotionDetector(pMotionDetector) {

}


void FireDetector::segment(const cv::Mat &bgrImage, cv::Mat &output) {
    cv::Mat blured;

    cv::GaussianBlur(bgrImage, blured, {3, 3}, 10);

    cv::Mat colorMask;
    pColorDetector->segment(blured, colorMask);

    cv::Mat motionMask;
    pMotionDetector->segment(blured, motionMask);

    initOutput(output, bgrImage.size());

    for (int i = 0; i < bgrImage.rows; ++i) {
        for (int k = 0; k < bgrImage.cols; ++k) {
            if (colorMask.at<uchar>(i, k) == kObjectMask && motionMask.at<uchar>(i, k) == kObjectMask) {
                output.at<uchar>(i, k) = kObjectMask;
            }
        }
    }
}



