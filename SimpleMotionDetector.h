#ifndef FIRE_SEGMENTATION_SIMPLEMOTIONDETECTOR_H
#define FIRE_SEGMENTATION_SIMPLEMOTIONDETECTOR_H

#include "Detector.h"

#include <list>

#include "CircularArray.h"


class SimpleMotionDetector : public Detector {
public:
    SimpleMotionDetector(int diffThreshold, int lookBackStep);
    int getDiffThreshold() const;
    void setDiffThreshold(int diffThreshold);
    int getLookBackStep() const;
    void setLookBackStep(int lookBackStep);
    void segment(const cv::Mat &bgrImage, cv::Mat &output) override;

private:
    static int getSaturation(const cv::Mat &hsvImage, int i, int k);

private:
    int diffThreshold;
    CircularArray<cv::Mat> prevHsvImages;

    bool canDetect() const;
    void detectMotion(const cv::Mat &hsvImage, cv::Mat &output) const;
    int getSaturationDiff(const cv::Mat &hsvImage, int row, int col) const;
    const cv::Mat &getControlHsvImage() const;
    void memorize(const cv::Mat &hsvImage);
};


#endif //FIRE_SEGMENTATION_SIMPLEMOTIONDETECTOR_H
