#include "DetectorWithMemory.h"


DetectorWithMemory::DetectorWithMemory(const cv::Ptr<Detector> &pDetector, unsigned int historyLength)
    : pDetector(pDetector),
      memory(historyLength) {}


const cv::Ptr<Detector> &DetectorWithMemory::getDetector() const {
    return pDetector;
}


void DetectorWithMemory::setDetector(const cv::Ptr<Detector> &pDetector) {
    DetectorWithMemory::pDetector = pDetector;
}


unsigned int DetectorWithMemory::getHistoryLength() const {
    return memory.getSize();
}


void DetectorWithMemory::setHistoryLength(unsigned int historyLength) {
    unsigned int excessCount = memory.getExcessCount(historyLength);
    for (unsigned int i = 0; i < excessCount; ++i) {
        removeFromAccumulator(memory[i]);
    }
    memory.setSize(historyLength);
}


void DetectorWithMemory::segment(const cv::Mat &bgrImage, cv::Mat &output) {
    if (accumulator.size() != bgrImage.size()) {
        initAccumulator(bgrImage.size());
        memory.clear();
    }

    cv::Mat mask;
    pDetector->segment(bgrImage, mask);
    memorize(mask);

    composeHistory(output);
}


void DetectorWithMemory::initAccumulator(const cv::Size &size) {
    accumulator = cv::Mat::zeros(size, CV_32S);
}


void DetectorWithMemory::composeHistory(cv::Mat &output) {
    initOutput(output, accumulator.size());

    for (int i = 0; i < accumulator.rows; ++i) {
        for (int j = 0; j < accumulator.cols; ++j) {
            if (0 < accumulator.at<int>(i, j)) {
                output.at<uchar>(i, j) = kObjectMask;
            }
        }
    }
}


void DetectorWithMemory::memorize(const cv::Mat &mask) {
    if (memory.isFull()) {
        forgetOldestMask();
    }
    memory.push_back(mask);
    addToAccumulator(mask);
}


void DetectorWithMemory::forgetOldestMask() {
    const auto &mask = memory.front();
    removeFromAccumulator(mask);
    memory.pop_front();
}


void DetectorWithMemory::addToAccumulator(const cv::Mat &mask) {
    for (int i = 0; i < accumulator.rows; ++i) {
        for (int k = 0; k < accumulator.cols; ++k) {
            if (kObjectMask == mask.at<uchar>(i, k)) {
                ++accumulator.at<int>(i, k);
            }
        }
    }
}


void DetectorWithMemory::removeFromAccumulator(const cv::Mat &mask) {
    for (int i = 0; i < accumulator.rows; ++i) {
        for (int k = 0; k < accumulator.cols; ++k) {
            if (kObjectMask == mask.at<uchar>(i, k)) {
                --accumulator.at<int>(i, k);
            }
        }
    }
}
