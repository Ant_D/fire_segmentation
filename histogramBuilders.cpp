#include "histogramBuilders.h"

void NaiveBayesBuilder::strengthenColor(const cv::Vec3b &color, Histogram &hist) const {
    hist.addByColor(color, 1);
}


GaussianNaiveBayesBuilder::GaussianNaiveBayesBuilder(int filterSize)
    : filterSize(filterSize),
      filter(filterSize, std::vector<std::vector<double>>(filterSize, std::vector<double>(filterSize, 0))) {
    init();
}


void GaussianNaiveBayesBuilder::init() {
    for (int i = 0; i < filterSize; ++i) {
        for (int j = 0; j < filterSize; ++j) {
            for (int k = 0; k < filterSize; ++k) {
                filter[i][j][k] = gauss(i - radius(), j - radius(), k - radius());
            }
        }
    }
}


void GaussianNaiveBayesBuilder::strengthenColor(const cv::Vec3b &color, Histogram &hist) const {
    int x;
    int y;
    int z;
    hist.colorToCoords(color, x, y, z);

    auto xRange = coordRange(x, hist.getBinstPerDim() - 1);
    auto yRange = coordRange(y, hist.getBinstPerDim() - 1);
    auto zRange = coordRange(z, hist.getBinstPerDim() - 1);

    for (int x1 = xRange.first; x1 <= xRange.second; ++x1) {
        for (int y1 = yRange.first; y1 <= yRange.second; ++y1) {
            for (int z1 = zRange.first; z1 <= zRange.second; ++z1) {
                hist.add(x1, y1, z1, filter[x1 - x + radius()][y1 - y + radius()][z1 - z + radius()]);
            }
        }
    }
}


std::pair<int, int> GaussianNaiveBayesBuilder::coordRange(int coord, int maxCoord) const {
    return {
            std::max(0, coord - radius()),
            std::min(maxCoord, coord + radius())
    };
}


int GaussianNaiveBayesBuilder::radius() const {
    return filterSize / 2;
}


double GaussianNaiveBayesBuilder::gauss(int x, int y, int z) const {
    return exp(-0.5 * (x * x + y * y + z * z)) / (2 * M_PI * sqrt(2 * M_PI));
}
