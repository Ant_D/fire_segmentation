#include "Detector.h"


void Detector::initOutput(cv::Mat &output, const cv::Size &size) {
    output = cv::Mat(size, CV_8UC1);
    for (int i = 0; i < output.rows; ++i) {
        for (int k = 0; k < output.cols; ++k) {
            output.at<uchar>(i, k) = kNonObjectMask;
        }
    }
}
