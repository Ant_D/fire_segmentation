#include "MotionDetector.h"


MotionDetector::MotionDetector(cv::Ptr<cv::BackgroundSubtractor> pBackgroundSubtractor)
    : pBackgroundSubtractor(pBackgroundSubtractor) {

}


void MotionDetector::segment(const cv::Mat &bgrImage, cv::Mat &output) {
    pBackgroundSubtractor->apply(bgrImage, output);
}


const cv::Ptr<cv::BackgroundSubtractor> &MotionDetector::getPBackgroundSubtractor() const {
    return pBackgroundSubtractor;
}


void MotionDetector::setPBackgroundSubtractor(const cv::Ptr<cv::BackgroundSubtractor> &pBackgroundSubtractor) {
    MotionDetector::pBackgroundSubtractor = pBackgroundSubtractor;
}
