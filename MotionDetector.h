#ifndef FIRE_SEGMENTATION_MOTIONDETECTOR_H
#define FIRE_SEGMENTATION_MOTIONDETECTOR_H

#include "Detector.h"

#include <opencv2/video.hpp>

class MotionDetector : public Detector {
public:
    explicit MotionDetector(cv::Ptr<cv::BackgroundSubtractor> pBackgroundSubtractor);
    void segment(const cv::Mat &bgrImage, cv::Mat &output) final;
    const cv::Ptr<cv::BackgroundSubtractor> &getPBackgroundSubtractor() const;
    void setPBackgroundSubtractor(const cv::Ptr<cv::BackgroundSubtractor> &pBackgroundSubtractor);

private:
    cv::Ptr<cv::BackgroundSubtractor> pBackgroundSubtractor;
};


#endif //FIRE_SEGMENTATION_MOTIONDETECTOR_H
