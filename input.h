#ifndef FIRE_SEGMENTATION_INPUT_H
#define FIRE_SEGMENTATION_INPUT_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>


bool readImage(cv::Mat &image, cv::ImreadModes mode = cv::IMREAD_COLOR);

bool readSample(cv::Mat &image, cv::Mat &mask);

#endif //FIRE_SEGMENTATION_INPUT_H
