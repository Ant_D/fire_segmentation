#include <fstream>
#include <iostream>

#include <opencv2/core/utility.hpp>

#include "ColorHistogramDetector.h"

using namespace std;


const char *kOptions =
        "{help h usage |       | print help info}"
        "{correct_mask c  |       | path of correct mask}"
        "{model_mask m |       | path of mask created by model}";


void die(const std::string &message) {
    cout << message << endl;
    exit(EXIT_FAILURE);
}


void readMask(const string &maskPath, cv::Mat &mask) {
    mask = cv::imread(maskPath, cv::IMREAD_GRAYSCALE);
    if (nullptr == mask.data) {
        die("Can not read mask: " + maskPath);
    }
}


double evaluateIOU(const cv::Mat &correctMask, const cv::Mat &modelMask) {
    int unionCount = 0;
    int intersectionCount = 0;

    for (int i = 0; i < correctMask.rows; ++i) {
        const auto &correctRow = correctMask.ptr(i);
        const auto &modelRow = modelMask.ptr(i);
        for (int k = 0; k < correctMask.cols; ++k) {
            if (kObjectMask == correctRow[k] || kObjectMask == modelRow[k]) {
                ++unionCount;
                if (kObjectMask == correctRow[k] && kObjectMask == modelRow[k]) {
                    ++intersectionCount;
                }
            }
        }
    }

    return unionCount != 0 ? 1.0 * intersectionCount / unionCount : 1.0;
}


int main(int argc, const char **argv) {
    ios_base::sync_with_stdio(false);

    cv::CommandLineParser parser(argc, argv, kOptions);
    parser.about("Calculate intersection over union (IOU)");

    if (parser.has("help")) {
        parser.printMessage();
        return 0;
    }

    string correctMaskPath = parser.get<string>("correct_mask");
    string modelMaskPath = parser.get<string>("model_mask");

    if (!parser.check()) {
        parser.printErrors();
        return EXIT_FAILURE;
    }

    cv::Mat correctMask;
    cv::Mat modelMask;
    readMask(correctMaskPath, correctMask);
    readMask(modelMaskPath, modelMask);

    if (correctMask.size() != modelMask.size()) {
        die("Sizes of masks are not equal");
    }

    double score = evaluateIOU(correctMask, modelMask);

    cout << score << "\n";

    return 0;
}