#ifndef FIRE_SEGMENTATION_DETECTORWITHMEMORY_H
#define FIRE_SEGMENTATION_DETECTORWITHMEMORY_H

#include "Detector.h"

#include "CircularArray.h"


class DetectorWithMemory : public Detector {
public:
    DetectorWithMemory(const cv::Ptr<Detector> &pDetector, unsigned int historyLength);
    const cv::Ptr<Detector> &getDetector() const;
    void setDetector(const cv::Ptr<Detector> &pDetector);
    unsigned int getHistoryLength() const;
    void setHistoryLength(unsigned int historyLength);
    void segment(const cv::Mat &bgrImage, cv::Mat &output) override;

private:
    cv::Ptr<Detector> pDetector;
    CircularArray<cv::Mat> memory;
    cv::Mat accumulator;

    void initAccumulator(const cv::Size &size);
    void composeHistory(cv::Mat &output);
    void memorize(const cv::Mat &mask);
    void forgetOldestMask();
    void addToAccumulator(const cv::Mat &mask);
    void removeFromAccumulator(const cv::Mat &mask);
};


#endif //FIRE_SEGMENTATION_DETECTORWITHMEMORY_H
