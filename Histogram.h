#ifndef FIRE_SEGMENTATION_HISTOGRAM_H
#define FIRE_SEGMENTATION_HISTOGRAM_H

#include <istream>
#include <ostream>
#include <vector>

#include <opencv2/core/core.hpp>


class Histogram {
public:
    using valueT = double;

public:
    static const int kColorValueCeil = 256;

public:
    explicit Histogram(int resolution);

    int getBinstPerDim() const;
    valueT get(int i, int j, int k) const;
    void colorToCoords(const cv::Vec3b &color, int &i, int &j, int &k);
    valueT getByColor(const cv::Vec3b &color);
    void set(int i, int j, int k, valueT newValue);

    void normalize();

    void add(int i, int j, int k, valueT value);
    void addByColor(const cv::Vec3b &color, valueT value);

    friend std::ostream &operator<<(std::ostream &outputStream, const Histogram &hist);
    friend std::istream &operator>>(std::istream &inputStream, Histogram &hist);

private:
    int resolution;
    int binsPerDim;
    std::vector<valueT> values;
};

#endif