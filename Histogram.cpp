#include "Histogram.h"


Histogram::Histogram(int resolution)
        : resolution(resolution),
          binsPerDim((kColorValueCeil + resolution - 1) / resolution),
          values(binsPerDim * binsPerDim * binsPerDim, 0) {}



int Histogram::getBinstPerDim() const {
    return binsPerDim;
}


Histogram::valueT Histogram::get(int i, int j, int k) const {
    return values[binsPerDim * binsPerDim * i + binsPerDim * j + k];
}


void Histogram::colorToCoords(const cv::Vec3b &color, int &i, int &j, int &k) {
    i = color[0] / resolution;
    j = color[1] / resolution;
    k = color[2] / resolution;
}


Histogram::valueT Histogram::getByColor(const cv::Vec3b &color) {
    int i = 0;
    int j = 0;
    int k = 0;
    colorToCoords(color, i, j, k);
    return get(i, j, k);
}


void Histogram::set(int i, int j, int k, Histogram::valueT newValue) {
    values[binsPerDim * binsPerDim * i + binsPerDim * j + k] = newValue;
}


void Histogram::normalize() {
    Histogram::valueT total = 0;
    for (auto value : values) {
        total += value;
    }
    for (auto &value : values) {
        value /= total;
    }
}



void Histogram::add(int i, int j, int k, Histogram::valueT value) {
    auto oldValue = get(i, j, k);
    set(i, j, k, oldValue + value);
}


void Histogram::addByColor(const cv::Vec3b &color, Histogram::valueT value) {
    int i;
    int j;
    int k;
    colorToCoords(color, i, j, k);
    add(i, j, k, value);
}


std::ostream &operator<<(std::ostream &outputStream, const Histogram &hist) {
    outputStream << hist.resolution << "\n";
    for (int i = 0; i < hist.binsPerDim; ++i) {
        for (int j = 0; j < hist.binsPerDim; ++j) {
            for (int k = 0; k < hist.binsPerDim; ++k) {
                outputStream << hist.get(i, j, k) << " ";
            }
            outputStream << "\n";
        }
    }
    return outputStream;
}

std::istream &operator>>(std::istream &inputStream, Histogram &hist) {
    Histogram::valueT value;
    int resolution;

    inputStream >> resolution;
    hist = Histogram(resolution);

    for (int i = 0; i < hist.binsPerDim; ++i) {
        for (int j = 0; j < hist.binsPerDim; ++j) {
            for (int k = 0; k < hist.binsPerDim; ++k) {
                inputStream >> value;
                hist.set(i, j, k, value);
            }
        }
    }

    return inputStream;
}
