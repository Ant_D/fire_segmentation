#include "input.h"

#include <iostream>


bool readImage(cv::Mat &image, cv::ImreadModes mode) {
    bool success = false;
    std::string imagePath;

    if (std::getline(std::cin, imagePath)) {
        image = cv::imread(imagePath, mode);
        if (nullptr != image.data) {
            success = true;
        } else {
            std::cerr << "Can't read image: " << imagePath << std::endl;
        }
    }

    return success;
}


bool readSample(cv::Mat &image, cv::Mat &mask) {
    bool imageIsRead = readImage(image);
    bool maskIsRead = readImage(mask, cv::IMREAD_GRAYSCALE);
    return imageIsRead && maskIsRead;
}