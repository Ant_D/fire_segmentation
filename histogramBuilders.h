#ifndef FIRE_SEGMENTATION_HISTOGRAM_BUILDERS_H
#define FIRE_SEGMENTATION_HISTOGRAM_BUILDERS_H

#define _USE_MATH_DEFINES

#include <vector>
#include <utility>
#include <cmath>

#include <opencv2/core/core.hpp>

#include "Histogram.h"


class HistogramBuilder {
public:
    virtual void strengthenColor(const cv::Vec3b &color, Histogram &hist) const = 0;
};


class NaiveBayesBuilder : public HistogramBuilder {
public:
    void strengthenColor(const cv::Vec3b &color, Histogram &hist) const override;
};


class GaussianNaiveBayesBuilder : public HistogramBuilder {
public:
    explicit GaussianNaiveBayesBuilder(int filterSize);

    void init();

    void strengthenColor(const cv::Vec3b &color, Histogram &hist) const override;

private:
    int filterSize;
    std::vector<std::vector<std::vector<double>>> filter;

    std::pair<int, int> coordRange(int coord, int maxCoord) const;

    int radius() const;

    double gauss(int x, int y, int z) const;
};

#endif
