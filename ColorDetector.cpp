#include "ColorDetector.h"

#include <opencv2/imgproc/imgproc.hpp>


const int kMaxColorValue = 255;
const int kMaxSaturationValue = 100;


ColorDetector::ColorDetector(uchar redThreshold, uchar saturationThreshold) : redThreshold(redThreshold), saturationThreshold(saturationThreshold) {

}


void ColorDetector::segment(const cv::Mat &bgrImage, cv::Mat &output) {
    initOutput(output, bgrImage.size());
    cv::Mat hsvImage;
    cvtColor(bgrImage, hsvImage, cv::ColorConversionCodes::COLOR_BGR2HSV);

    cv::Vec3b bgrColor;
    cv::Vec3b hsvColor;
    for (int i = 0; i < bgrImage.rows; ++i) {
        for (int k = 0; k < bgrImage.cols; ++k) {
            bgrColor = bgrImage.at<cv::Vec3b>(i, k);
            hsvColor = hsvImage.at<cv::Vec3b>(i, k);
            uchar red = bgrColor[2];
            uchar green = bgrColor[1];
            uchar blue = bgrColor[0];
            uchar saturation = hsvColor[1];
            bool isFirePixel = red > green && green >= blue &&
                red > redThreshold &&
                redThreshold > 0 &&
                saturation >= 1.0 * (kMaxColorValue - red) * saturationThreshold / redThreshold;
            if (isFirePixel) {
                output.at<uchar>(i, k) = kObjectMask;
            }
        }
    }
}


void ColorDetector::setRedThreshold(int redThreshold) {
    this->redThreshold = redThreshold;
}


void ColorDetector::setSaturationThreshold(int saturationThreshold) {
    this->saturationThreshold = saturationThreshold;
}


int ColorDetector::getRedThreshold() const {
    return redThreshold;
}


int ColorDetector::getSaturationThreshold() const {
    return saturationThreshold;
}
