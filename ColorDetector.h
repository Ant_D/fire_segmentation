#ifndef FIRE_SEGMENTATION_COLORDETECTOR_H
#define FIRE_SEGMENTATION_COLORDETECTOR_H

#include "Detector.h"

#include <opencv2/core/core.hpp>


class ColorDetector : public Detector {
public:
    static const int kDefaultRedThreshold = 170;
    static const int kDefaultSaturationThreshold = 60;

public:
    explicit ColorDetector(uchar redThreshold = kDefaultRedThreshold, uchar saturationThreshold = kDefaultSaturationThreshold);
    void segment(const cv::Mat &bgrImage, cv::Mat &output) final;
    int getRedThreshold() const;
    void setRedThreshold(int redThreshold);
    int getSaturationThreshold() const;
    void setSaturationThreshold(int saturationThreshold);

private:
    uchar redThreshold;
    uchar saturationThreshold;
};

#endif //FIRE_SEGMENTATION_COLORDETECTOR_H
