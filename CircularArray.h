#ifndef FIRE_SEGMENTATION_CIRCULARARRAY_H
#define FIRE_SEGMENTATION_CIRCULARARRAY_H


#include <vector>

#include <opencv2/core/core.hpp>

template <class T>
class CircularArray {
public:
    explicit CircularArray(unsigned int size = 0);
    virtual void push_back(const T &value);
    virtual void pop_front();
    const T &front() const;
    const T &back() const;
    unsigned int getSize() const;
    void setSize(unsigned int size);
    unsigned int getCount() const;
    bool isEmpty() const;
    bool isFull() const;
    T &operator [](unsigned int index);
    const T &operator [](unsigned int index) const;
    void clear();
    unsigned int getExcessCount(unsigned int size) const;

private:
    unsigned int count;
    unsigned int start;
    std::vector<T> array;

    unsigned int getIndexOf(unsigned int positionFromStart) const;
};


template <class T>
CircularArray<T>::CircularArray(unsigned int size)
        : count(0), start(0), array(size) {}


template <class T>
void CircularArray<T>::push_back(const T &value) {
    if (isFull()) {
        pop_front();
    }
    array[getIndexOf(count)] = value;
    ++count;
}


template <class T>
void CircularArray<T>::pop_front() {
    start = getIndexOf(1);
    --count;
}


template <class T>
const T &CircularArray<T>::front() const {
    return array[start];
}


template <class T>
const T &CircularArray<T>::back() const {
    return array[getIndexOf(count - 1)];
}


template <class T>
unsigned int CircularArray<T>::getSize() const {
    return array.size();
}


template <class T>
void CircularArray<T>::setSize(unsigned int size) {
    unsigned int excessCount = getExcessCount(size);

    std::vector<T> tmp(size);
    for (unsigned int i = 0; i < count - excessCount; ++i) {
        tmp[i] = array[getIndexOf(i + excessCount)];
    }

    array.swap(tmp);
    count -= excessCount;
    start = 0;
}


template <class T>
unsigned int CircularArray<T>::getCount() const {
    return count;
}


template <class T>
bool CircularArray<T>::isEmpty() const {
    return count == 0;
}


template <class T>
bool CircularArray<T>::isFull() const {
    return count >= array.size();
}


template <class T>
T &CircularArray<T>::operator[](unsigned int index) {
    return array[getIndexOf(index)];
}


template<class T>
const T &CircularArray<T>::operator[](unsigned int index) const {
    return array[getIndexOf(index)];
}


template <class T>
void CircularArray<T>::clear() {
    count = 0;
}


template <class T>
unsigned int CircularArray<T>::getExcessCount(unsigned int size) const {
    return (count <= size ? 0 : count - size);
}


template <class T>
unsigned int CircularArray<T>::getIndexOf(unsigned int positionFromStart) const {
    return (start + positionFromStart) % getSize();
}


#endif //FIRE_SEGMENTATION_CIRCULARARRAY_H
