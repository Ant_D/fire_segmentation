#ifndef FIRE_SEGMENTATION_FIREDETECTOR_H
#define FIRE_SEGMENTATION_FIREDETECTOR_H

#include "Detector.h"


class FireDetector : public Detector {
public:
    FireDetector(cv::Ptr<Detector> pColorDetector, cv::Ptr<Detector> pMotionDetector);
    void segment(const cv::Mat &bgrImage, cv::Mat &output) final;
private:
    cv::Ptr<Detector> pColorDetector;
    cv::Ptr<Detector> pMotionDetector;
};


#endif //FIRE_SEGMENTATION_FIREDETECTOR_H
