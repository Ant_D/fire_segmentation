#include <fstream>
#include <iostream>

#include <opencv2/core/utility.hpp>

#include "ColorDetector.h"
#include "ColorHistogramDetector.h"
#include "FireDetector.h"
#include "MotionDetector.h"

using namespace std;


const char *kOptions =
    "{help h usage  |       | print help info}"
    "{model_path p  | model | model path}"
    "{video_path v  | 0     | path to input video}";


void die(const std::string &message) {
    cout << message << endl;
    exit(EXIT_FAILURE);
}


void openVideoCapture(cv::VideoCapture &video, const string videoPath) {
    if (videoPath != "0") {
        video.open(videoPath);
    }
    else {
        video.open(0);
    }
    if (!video.isOpened()) {
        die("Can not open video: " + videoPath);
    }
}


void mainloop(cv::VideoCapture &video, const cv::Ptr<Detector> &pDetector) {
    cv::namedWindow("Original", cv::WINDOW_AUTOSIZE);
    cv::namedWindow("Result", cv::WINDOW_AUTOSIZE);

    cv::Mat original;
    cv::Mat result;
    while (video.read(original)) {
        pDetector->segment(original, result);

        cv::imshow("Original", original);
        cv::imshow("Result", result);

        if (cv::waitKey(50) == 27) {
            break;
        }
    }

    cv::destroyAllWindows();
}


int main(int argc, const char **argv) {
    ios_base::sync_with_stdio(false);

    cv::CommandLineParser parser(argc, argv, kOptions);
    parser.about("Segment object in videostream");

    if (parser.has("help")) {
        parser.printMessage();
        return 0;
    }

    string modelPath = parser.get<string>("model_path");
    string videoPath = parser.get<string>("video_path");

    if (!parser.check()) {
        parser.printErrors();
        return EXIT_FAILURE;
    }

    cv::Ptr<cv::BackgroundSubtractor> pBackgroundSubtractor = cv::createBackgroundSubtractorKNN();
    cv::Ptr<Detector> pMotionDetector = cv::makePtr<MotionDetector>(pBackgroundSubtractor);
    cv::Ptr<Detector> pColorDetector = ColorHistogramDetector::load(modelPath);
    cv::Ptr<Detector> pDetector = cv::makePtr<FireDetector>(pColorDetector, pMotionDetector);

    cv::VideoCapture video;
    openVideoCapture(video, videoPath);

    mainloop(video, pDetector);

    return 0;
}