#include "SimpleMotionDetector.h"

#include <opencv2/imgproc/imgproc.hpp>


SimpleMotionDetector::SimpleMotionDetector(int diffThreshold, int lookBackStep)
    : diffThreshold(diffThreshold),
    prevHsvImages(lookBackStep) {}


int SimpleMotionDetector::getDiffThreshold() const {
    return diffThreshold;
}


void SimpleMotionDetector::setDiffThreshold(int diffThreshold) {
    SimpleMotionDetector::diffThreshold = diffThreshold;
}


int SimpleMotionDetector::getLookBackStep() const {
    return prevHsvImages.getSize();
}


void SimpleMotionDetector::setLookBackStep(int lookBackStep) {
    prevHsvImages.setSize(lookBackStep);
}


void SimpleMotionDetector::segment(const cv::Mat &bgrImage, cv::Mat &output) {
    cv::Mat hsvImage;
    cv::cvtColor(bgrImage, hsvImage, cv::ColorConversionCodes::COLOR_BGR2HSV);

    initOutput(output, bgrImage.size());

    if (canDetect()) {
        detectMotion(hsvImage, output);
    }

    memorize(hsvImage);
}


int SimpleMotionDetector::getSaturation(const cv::Mat &hsvImage, int i, int k) {
    return hsvImage.at<cv::Vec3b>(i, k)[1];
}


bool SimpleMotionDetector::canDetect() const {
    return prevHsvImages.isFull();
}


void SimpleMotionDetector::detectMotion(const cv::Mat &hsvImage, cv::Mat &output) const {
    for (int i = 0; i < hsvImage.rows; ++i) {
        for (int k = 0; k < hsvImage.cols; ++k) {
            if (diffThreshold <= getSaturationDiff(hsvImage, i, k)) {
                output.at<uchar>(i, k) = kObjectMask;
            }
        }
    }
}


int SimpleMotionDetector::getSaturationDiff(const cv::Mat &hsvImage, int row, int col) const {
    const auto &controlHsvImage = getControlHsvImage();
    int diff = getSaturation(controlHsvImage, row, col) - getSaturation(hsvImage, row, col);
    diff = diff > 0 ? diff : -diff;
    diff /= prevHsvImages.getSize();

    return diff;
}


const cv::Mat &SimpleMotionDetector::getControlHsvImage() const {
    return prevHsvImages.front();
}


void SimpleMotionDetector::memorize(const cv::Mat &hsvImage) {
    prevHsvImages.push_back(hsvImage);
}
