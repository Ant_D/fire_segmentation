#include <fstream>
#include <iostream>

#include <opencv2/core/utility.hpp>

#include "ColorHistogramDetector.h"

using namespace std;


const char *kOptions =
    "{help h usage  |       | print help info}"
    "{model_path p  | model | model path}"
    "{image_path i  |       | path of input image}"
    "{output_path o |       | path where to save result}";


void die(const std::string &message) {
    cout << message << endl;
    exit(EXIT_FAILURE);
}


void showResult(const cv::Mat &original, const cv::Mat &result) {
    cv::namedWindow("Original", cv::WINDOW_AUTOSIZE);
    cv::namedWindow("Result", cv::WINDOW_AUTOSIZE);

    while (true) {
        cv::imshow("Original", original);
        cv::imshow("Result", result);

        if (cv::waitKey(50) == 27) {
            break;
        }
    }

    cv::destroyAllWindows();
}


int main(int argc, const char **argv) {
    ios_base::sync_with_stdio(false);

    cv::CommandLineParser parser(argc, argv, kOptions);
    parser.about("Segment object in image");

    if (parser.has("help")) {
        parser.printMessage();
        return 0;
    }

    string modelPath = parser.get<string>("model_path");
    string imagePath = parser.get<string>("image_path");
    bool bShowResult = !parser.has("output_path");
    string outputPath = parser.get<string>("output_path");

    if (!parser.check()) {
        parser.printErrors();
        return EXIT_FAILURE;
    }

    cv::Ptr<Detector> pDetector = ColorHistogramDetector::load(modelPath);

    cv::Mat original = cv::imread(imagePath);
    if (nullptr == original.data) {
        die("Can not read image: " + imagePath);
    }

    cv::Mat result;
    pDetector->segment(original, result);

    if (bShowResult) {
        showResult(original, result);
    }
    else {
        cv::imwrite(outputPath, result);
    }

    return 0;
}