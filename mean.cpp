#include <iostream>
#include <vector>

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);

    vector<double> values;
    double value;

    while (cin >> value) {
        values.push_back(value);
    }

    double totalValue = 0;
    for (auto value: values) {
        totalValue += value;
    }
    double meanValue = !values.empty() ? totalValue / values.size() : 0.0;

    cout << meanValue << "\n";

    return 0;
}